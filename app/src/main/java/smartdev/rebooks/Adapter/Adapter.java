package smartdev.rebooks.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import smartdev.rebooks.DetailActivity;
import smartdev.rebooks.Main_Menu;
import smartdev.rebooks.Model.Books;
import smartdev.rebooks.Model.Genre;
import smartdev.rebooks.Model.Image;
import smartdev.rebooks.Model.User;
import smartdev.rebooks.R;

import static android.content.ContentValues.TAG;

/**
 * Created by jih00d on 1/9/18.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.AdapterViewHolder> {

    private Context mcontext;
    private List<Books> datalist;
    private List<Books> mFilteredList;

    public Adapter(Context context, List<Books> arrayList) {
        this.datalist = arrayList;
        this.mFilteredList = arrayList;
        this.mcontext = context;
    }

    @Override
    public AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_activity, parent,false);

        return new AdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterViewHolder holder, int position) {
        holder.title.setText(datalist.get(position).getTitle());
        holder.price.setText("Rp. " + datalist.get(position).getPrice());


        String sGenre = "";

        for (Genre genre : datalist.get(position).getGenres()) {
            sGenre += genre.getName()+", ";
        }
        holder.genre.setText(sGenre);
        
        Glide.with(mcontext).load(datalist.get(position).getCover()).placeholder(R.drawable.ic_timelapse_black_24dp).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class AdapterViewHolder extends RecyclerView.ViewHolder {
        TextView title, price, genre;
        ImageView imageView;

        public AdapterViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.txt_title);
            genre = (TextView) itemView.findViewById(R.id.txt_genre);
            price = (TextView) itemView.findViewById(R.id.txt_price);
            imageView = (ImageView) itemView.findViewById(R.id.image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION){

                        Books clickedDataItem = datalist.get(pos);
                        Intent intent = new Intent(mcontext, DetailActivity.class);
                        intent.putExtra("cover", datalist.get(pos).getCover());
                        intent.putExtra("title", datalist.get(pos).getTitle());
                        intent.putExtra("description", datalist.get(pos).getDescription());
                        intent.putExtra("createdAt",datalist.get(pos).getCreatedAt());
                        intent.putExtra("condition", datalist.get(pos).getCondition());
                        intent.putExtra("authorBook",datalist.get(pos).getAuthorBook());
                        intent.putExtra("publisherBook", datalist.get(pos).getPublisherBook());
                        intent.putExtra("publicationBook", datalist.get(pos).getPublicationYearBook().toString());
                        intent.putExtra("price", datalist.get(pos).getPrice());
                        intent.putExtra("user", datalist.get(pos).getUser().toString());
                        String sGenre = "";
                        for (Genre genre : datalist.get(pos).getGenres()){
                            sGenre += genre.getName()+", ";
                        }
                        intent.putExtra("genre",sGenre);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mcontext.startActivity(intent);
                    }
                }
            });
        }
    }

    public Filter getFilter(){
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charstring = charSequence.toString();

                if (charstring.isEmpty())
                    datalist = mFilteredList;
                else {
                    ArrayList<Books> filteredList = new ArrayList<>();

                    for (Books books : mFilteredList){
                        if (books.getTitle().toLowerCase().contains(charstring) || books.getPrice().toLowerCase().contains(charstring)) {
                            filteredList.add(books);
                        }
                    }
                    datalist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = datalist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                datalist = (ArrayList<Books>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
