package smartdev.rebooks.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jih00d on 1/9/18.
 */

public class BooksList {

    @SerializedName("books")
    @Expose
    private List<Books> books = null;

    public List<Books> getBooks() {
        return books;
    }

    public void setBooks(List<Books> books) {
        this.books = books;
    }

}
