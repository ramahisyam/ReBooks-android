package smartdev.rebooks;

import android.content.Intent;
import android.os.TestLooperManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;

import smartdev.rebooks.Model.Image;

public class DetailActivity extends AppCompatActivity {

    private ImageView cover;
    private TextView title;
    private TextView description;
    private TextView createdAt;
    private TextView condition;
    private TextView authorBook;
    private TextView publisherBook;
    private TextView publicationBook;
    private TextView genre;
    private TextView user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        android.support.v7.widget.Toolbar toolbarDtl= (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_detail);
        setSupportActionBar(toolbarDtl);

        cover = (ImageView) findViewById(R.id.imgCover);
        title = (TextView) findViewById(R.id.dtlTitle);
        description = (TextView) findViewById(R.id.dtlDesc);
        genre = (TextView) findViewById(R.id.dtlGenre);
        createdAt = (TextView) findViewById(R.id.dtlCreateAt);
        condition = (TextView) findViewById(R.id.dtlCondition);
        authorBook = (TextView) findViewById(R.id.dtlAuthor);
        publisherBook = (TextView) findViewById(R.id.dtlPublisher);
        publicationBook = (TextView) findViewById(R.id.dtlPublication);
        user = (TextView) findViewById(R.id.dtlUser);


        Intent intent = getIntent();
        if (intent.hasExtra("title")){
            String mcover = getIntent().getExtras().getString("cover");
            String mtitle = getIntent().getExtras().getString("title");
            String mdescription = getIntent().getExtras().getString("description");
            String mGenre = getIntent().getExtras().getString("genre");
            String mcreatedAt = getIntent().getExtras().getString("createdAt");
            String mcondition = getIntent().getExtras().getString("condition");
            String mauthorBook = getIntent().getExtras().getString("authorBook");
            String mpublisherBook = getIntent().getExtras().getString("publisherBook");
            String mpublicationBook = getIntent().getExtras().getString("publicationBook");
            String muser = getIntent().getExtras().getString("user");
            Glide.with(this).load(mcover).into(cover);

            title.setText(mtitle);
            description.setText(mdescription);
            genre.setText(mGenre);
            createdAt.setText(mcreatedAt);
            condition.setText(mcondition);
            authorBook.setText(mauthorBook);
            publisherBook.setText(mpublisherBook);
            publicationBook.setText(mpublicationBook);
            user.setText(muser);
        }

    }
}
