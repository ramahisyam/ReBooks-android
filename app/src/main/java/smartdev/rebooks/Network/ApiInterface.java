package smartdev.rebooks.Network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import smartdev.rebooks.Model.BooksList;
import smartdev.rebooks.Model.Genre;

/**
 * Created by jih00d on 1/9/18.
 */

public interface ApiInterface {
    @GET("api/books")
    Call<BooksList> getBooksData(@Query("company_no")int ApiKey);
}
